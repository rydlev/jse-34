package ru.t1.rydlev.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ServerErrorResponse extends AbstractResultResponse {

    public ServerErrorResponse() {
        setSuccess(false);
    }

    public ServerErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
