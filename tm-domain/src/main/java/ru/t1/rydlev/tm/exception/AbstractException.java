package ru.t1.rydlev.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String s) {
        super(s);
    }

    public AbstractException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AbstractException(Throwable throwable) {
        super(throwable);
    }

    public AbstractException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}
