package ru.t1.rydlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.Result;
import ru.t1.rydlev.tm.dto.request.ServerAboutRequest;
import ru.t1.rydlev.tm.dto.request.ServerVersionRequest;
import ru.t1.rydlev.tm.dto.response.ServerAboutResponse;
import ru.t1.rydlev.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/SystemEndpoint")
public interface ISystemEndpoint extends IEndpoint {

    @POST
    @NotNull
    @WebMethod
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Result ping();

    @POST
    @NotNull
    @WebMethod
    @Path("/getAbout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerAboutRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/getVersion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerVersionRequest request
    );

}
