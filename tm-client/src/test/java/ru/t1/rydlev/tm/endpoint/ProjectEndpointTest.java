package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rydlev.tm.api.client.IAuthEndpointClient;
import ru.t1.rydlev.tm.api.client.IProjectEndpointClient;
import ru.t1.rydlev.tm.api.client.IUserEndpointClient;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IProjectEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpoint;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.marker.IntegrationCategory;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.service.PropertyService;

import java.util.List;

import static ru.t1.rydlev.tm.constant.ProjectTestData.*;
import static ru.t1.rydlev.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT_CLIENT = IUserEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT_CLIENT = IProjectEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    private int projectIndex1;

    @Nullable
    private String projectId2;

    private int projectIndex2;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = AUTH_ENDPOINT_CLIENT.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        USER_ENDPOINT_CLIENT.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = AUTH_ENDPOINT_CLIENT.loginUser(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        USER_ENDPOINT_CLIENT.removeUser(request);
    }

    @Before
    public void before() {
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
        projectIndex1 = 0;
        projectId2 = createTestProject(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);
        projectIndex2 = 1;
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.clearProject(request));
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return PROJECT_ENDPOINT_CLIENT.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private Project findTestProjectById(final String id) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(id);
        return PROJECT_ENDPOINT_CLIENT.showProjectById(request).getProject();
    }

    @Test
    public void changeStatusByIdProject() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setId(projectId1);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> PROJECT_ENDPOINT_CLIENT.changeProjectStatusById(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setId(projectId1);
        request.setStatus(status);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.changeProjectStatusById(request));
        @Nullable final Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void changeStatusByIndexProject() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setStatus(status);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.changeProjectStatusByIndex(request));
        @Nullable final Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.clearProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNull(project);
        project = findTestProjectById(projectId2);
        Assert.assertNull(project);
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(USER_PROJECT3_NAME);
        projectCreateRequest.setDescription(USER_PROJECT3_DESCRIPTION);
        @Nullable Project project = PROJECT_ENDPOINT_CLIENT.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @Test
    public void completeByIdProject() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.completeProjectById(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void completeByIndexProject() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.completeProjectByIndex(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void listProject() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<Project> projects = PROJECT_ENDPOINT_CLIENT.listProject(request).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        for (Project project : projects) {
            Assert.assertNotNull(findTestProjectById(project.getId()));
        }
    }

    @Test
    public void removeByIdProject() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setId(projectId2);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.removeProjectById(request));
        Assert.assertNull(findTestProjectById(projectId2));
    }

    @Test
    public void removeByIndexProject() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken);
        request.setIndex(projectIndex2);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.removeProjectByIndex(request));
        Assert.assertNull(findTestProjectById(projectId2));
    }

    @Test
    public void showByIdProject() {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(projectId1);
        @Nullable final Project project = PROJECT_ENDPOINT_CLIENT.showProjectById(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void showByIndexProject() {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        @Nullable final Project project = PROJECT_ENDPOINT_CLIENT.showProjectByIndex(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void startByIdProject() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.startProjectById(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByIndexProject() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.startProjectByIndex(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void updateByIdProject() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setId(projectId1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.updateProjectById(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @Test
    public void updateByIndexProject() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(PROJECT_ENDPOINT_CLIENT.updateProjectByIndex(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

}
