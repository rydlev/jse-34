package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rydlev.tm.api.client.IAuthEndpointClient;
import ru.t1.rydlev.tm.api.client.IDomainEndpointClient;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.marker.IntegrationCategory;
import ru.t1.rydlev.tm.service.PropertyService;

import static ru.t1.rydlev.tm.constant.UserTestData.ADMIN_LOGIN;
import static ru.t1.rydlev.tm.constant.UserTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IDomainEndpoint DOMAIN_ENDPOINT_CLIENT = IDomainEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = AUTH_ENDPOINT_CLIENT.loginUser(loginRequest).getToken();
    }

    @Test
    public void backupSaveData() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBackup(request));
    }

    @Test
    public void backupLoadData() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBackup(request));
    }

    @Test
    @Ignore
    public void base64SaveData() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBase64(request));
    }

    @Test
    @Ignore
    public void base64LoadData() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBase64(request));
    }

    @Test
    @Ignore
    public void binarySaveData() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBinary(request));
    }

    @Test
    @Ignore
    public void binaryLoadData() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBinary(request));
    }

    @Test
    public void jsonSaveFasterXmlData() {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonFasterXml(request));
    }

    @Test
    public void jsonLoadFasterXmlData() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataJsonFasterXml(request));
    }

    @Test
    public void jsonSaveJaxBData() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonJaxB(request));
    }

    @Test
    public void jsonLoadJaxBData() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataJsonJaxB(request));
    }

    @Test
    @Ignore
    public void xmlSaveFasterXmlData() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlFasterXml(request));
    }

    @Test
    @Ignore
    public void xmlLoadFasterXmlData() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataXmlFasterXml(request));
    }

    @Test
    public void xmlSaveJaxBData() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlJaxB(request));
    }

    @Test
    public void xmlLoadJaxBData() {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataXmlJaxB(request));
    }

    @Test
    @Ignore
    public void yamlSaveFasterXmlData() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataYamlFasterXml(request));
    }

    @Test
    @Ignore
    public void yamlLoadFasterXmlData() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataYamlFasterXml(request));
    }

}
