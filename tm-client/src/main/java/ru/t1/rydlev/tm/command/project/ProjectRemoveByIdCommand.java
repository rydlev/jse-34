package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().removeProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

}
