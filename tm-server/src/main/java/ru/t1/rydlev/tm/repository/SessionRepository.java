package ru.t1.rydlev.tm.repository;

import ru.t1.rydlev.tm.api.repository.ISessionRepository;
import ru.t1.rydlev.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}

